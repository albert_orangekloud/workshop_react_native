/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, useEffect, createContext, useMemo } from "react";
import {
    Button,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    useColorScheme,
    View,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { Colors } from "react-native/Libraries/NewAppScreen";

import { HomeStackParamList } from "./screen/types";
import BottomTabNavigation from "./screen/BottomTabNavigation";
import GalleryDetailScreen from "./screen/GalleryDetailScreen";
import AsyncStorage from "./utils/AsyncStorage";

interface UserContextInterface {
    userName: string;
    setUserName: React.Dispatch<React.SetStateAction<string>>;
}

export const UserContext = createContext<UserContextInterface>({
    userName: "",
    setUserName: () => {},
});

const App = () => {
    const isDarkMode = useColorScheme() === "dark";
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : "#ffffff",
        flex: 1,
    };
    const Stack = createStackNavigator<HomeStackParamList>();
    const [userName, setUserName] = useState("");
    const value = useMemo(() => ({ userName, setUserName }), [userName]);

    useEffect(() => {
        AsyncStorage.getStringData("userName")
            .then((value) => {
                if (value) {
                    setUserName(value ?? "");
                }
            })
            .catch((_err) => {});
    }, []);

    return (
        <SafeAreaView style={backgroundStyle}>
            <StatusBar
                barStyle={isDarkMode ? "light-content" : "dark-content"}
            />
            <UserContext.Provider value={value}>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="BottomTab">
                        <Stack.Screen
                            name="BottomTab"
                            component={BottomTabNavigation}
                            options={{ header: () => null }}
                        />
                        <Stack.Screen
                            name="GalleryDetail"
                            component={GalleryDetailScreen}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </UserContext.Provider>
        </SafeAreaView>
    );
};

export default App;
