import React from "react";
import { StyleSheet, View, Text } from "react-native";

const Header: React.FC<{
    title: string;
}> = ({ title }) => {
    return (
        <View style={Styles.viewStyle}>
            <Text style={Styles.textStyle}>{title}</Text>
        </View>
    );
};

const Styles = StyleSheet.create({
    viewStyle: {
        backgroundColor: "#F8F8F8",
        justifyContent: "center",
        alignItems: "center",
        height: 60,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: "relative",
    },
    textStyle: {
        fontSize: 25,
    },
});

export default Header;
