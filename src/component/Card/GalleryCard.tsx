import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

import { useNavigation } from "@react-navigation/native";

import { GalleryData } from "../../screen/MainScreen";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ParamList } from "../../screen/types";

const GalleryCard: React.FC<GalleryData> = (props) => {
    const navigation = useNavigation<ParamList>();
    return (
        <TouchableOpacity
            onPress={() => {
                navigation.navigate({
                    name: "GalleryDetail",
                    params: {
                        title: props.title,
                        url: props.url,
                    },
                });
            }}
            style={Styles.containerStyle}
        >
            <>
                <View style={Styles.cardSectionStyle}>
                    <View style={Styles.thumbnailContainerStyle}>
                        <Image
                            style={Styles.thumbnailStyle}
                            source={{ uri: props.thumbnailUrl ?? "" }}
                        />
                    </View>
                    <View style={Styles.headerContentStyle}>
                        <Text style={Styles.headerTextStyle}>
                            {props.title}
                        </Text>
                    </View>
                </View>
                <View style={Styles.cardSectionStyle}>
                    <Image
                        style={Styles.imageStyle}
                        source={{ uri: props.url ?? "" }}
                    />
                </View>
            </>
        </TouchableOpacity>
    );
};

const Styles = StyleSheet.create({
    containerStyle: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    cardSectionStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: "#fff",
        justifyContent: "flex-start",
        flexDirection: "row",
        borderColor: "#ddd",
        position: "relative",
    },
    thumbnailStyle: {
        height: 50,
        width: 50,
    },
    headerContentStyle: {
        justifyContent: "space-around",
        flex: 1,
    },
    headerTextStyle: {
        fontSize: 18,
    },
    thumbnailContainerStyle: {
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10,
        marginRight: 10,
    },
    imageStyle: {
        height: 300,
        flex: 1,
    },
});

export default GalleryCard;
