import React from "react";
import { FlatList } from "react-native";
import { GalleryData } from "../../screen/MainScreen";
import GalleryCard from "../Card/GalleryCard";

const AlbumList: React.FC<{ albumList: GalleryData[] }> = ({ albumList }) => {
    const renderItem = ({ item }: { item: GalleryData }) => (
        <GalleryCard
            id={item.id}
            title={item.title}
            thumbnailUrl={item.thumbnailUrl}
            url={item.url}
        />
    );

    return (
        <FlatList
            data={albumList}
            renderItem={renderItem}
            keyExtractor={(item) => item?.id ?? ""}
        />
    );
};

export default AlbumList;
