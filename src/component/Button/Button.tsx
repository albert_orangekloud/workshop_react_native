import React, { useContext } from "react";
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    Linking,
} from "react-native";
import { UserContext } from "../../App";

const Button: React.FC = () => {
    const { buttonStyle, textStyle } = Styles;
    const { userName } = useContext(UserContext);
    return (
        <TouchableOpacity
            onPress={() => Linking.openURL("https://orangekloud.com/emobiq/")}
            style={buttonStyle}
        >
            <Text style={textStyle}>
                {userName.length === 0 ? "" : userName + ", "}Click To See
                Website
            </Text>
        </TouchableOpacity>
    );
};

const Styles = StyleSheet.create({
    buttonStyle: {
        alignSelf: "stretch",
        backgroundColor: "#fff",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#007aff",
        marginLeft: 5,
        marginRight: 5,
    },
    textStyle: {
        alignSelf: "center",
        color: "#007aff",
        fontSize: 16,
        fontWeight: "600",
        paddingTop: 10,
        paddingBottom: 10,
    },
});

export default Button;
