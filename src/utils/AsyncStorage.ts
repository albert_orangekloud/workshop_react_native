import AsyncStorage from "@react-native-async-storage/async-storage";

export default {
    getStringData: async (key: string) => {
        try {
            return await AsyncStorage.getItem(key);
        } catch (e) {
            new Error("error");
        }
    },
    getObjectData: async (key: string) => {
        try {
            const jsonValue = await AsyncStorage.getItem(key);
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            new Error("error");
        }
    },
    storeData: async (key: string, value: string) => {
        try {
            await AsyncStorage.setItem(key, value);
        } catch (e) {
            new Error("error");
        }
    },
    storeObjectData: async (key: string, value: object) => {
        try {
            const jsonValue = JSON.stringify(value);
            await AsyncStorage.setItem(key, jsonValue);
        } catch (e) {
            new Error("error");
        }
    },
};
