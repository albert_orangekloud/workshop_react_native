import axios from "axios";
import React, { useState, useEffect, useContext } from "react";
import {
    Button,
    TextInput,
    useColorScheme,
    View,
    StyleSheet,
} from "react-native";

import { Colors } from "react-native/Libraries/NewAppScreen";
import { UserContext } from "../App";
import Header from "../component/Header/Header";
import AsyncStorage from "../utils/AsyncStorage";

const AccountScreen = () => {
    const isDarkMode = useColorScheme() === "dark";
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };
    const { userName, setUserName } = useContext(UserContext);

    const [textSearch, setTextSearch] = useState("");

    const savingUserName = () => {
        setUserName(textSearch);
        AsyncStorage.storeData("userName", textSearch);
    };

    return (
        <View style={backgroundStyle}>
            <Header title={userName + " Profile"} />
            <View
                style={{
                    backgroundColor: isDarkMode ? Colors.black : Colors.white,
                }}
            >
                <View style={Styles.formContainerStyle}>
                    <TextInput
                        onChangeText={setTextSearch}
                        value={textSearch}
                        placeholder="Change your username here"
                        style={Styles.textInputStyle}
                    />
                    <Button title="Change" onPress={savingUserName} />
                </View>
            </View>
        </View>
    );
};

const Styles = StyleSheet.create({
    formContainerStyle: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
    },
    textInputStyle: {
        flex: 1,
        borderColor: "#efefef",
        borderWidth: 1,
    },
});

export default AccountScreen;
