import axios from "axios";
import React, { useState, useEffect, useContext } from "react";
import {
    Button,
    TextInput,
    useColorScheme,
    View,
    StyleSheet,
} from "react-native";

import { Colors } from "react-native/Libraries/NewAppScreen";
import { UserContext } from "../App";
import Header from "../component/Header/Header";
import AlbumList from "../component/List/AlbumList";

export interface GalleryData {
    id: string | null | undefined;
    title: string | null | undefined;
    thumbnailUrl: string | null | undefined;
    url: string | null | undefined;
}

const MainScreen = () => {
    const isDarkMode = useColorScheme() === "dark";
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };
    const { userName } = useContext(UserContext);

    const [textSearch, setTextSearch] = useState("");
    const [isFiltered, setIsFiltered] = useState(false);
    const [albumList, setAlbumList] = useState<GalleryData[]>([]);
    const [filteredList, setFilteredList] = useState<GalleryData[]>([]);

    useEffect(() => {
        axios
            .get<GalleryData[]>("https://jsonplaceholder.typicode.com/photos")
            .then((response) => {
                setAlbumList(response.data);
                setFilteredList(response.data);
            });
    }, []);

    const filteringList = () => {
        setFilteredList(
            albumList.filter((item) => {
                return item.title?.includes(textSearch);
            })
        );
        setIsFiltered(true);
    };

    return (
        <View style={backgroundStyle}>
            <Header title={userName + " Album"} />
            <View
                style={{
                    backgroundColor: isDarkMode ? Colors.black : Colors.white,
                }}
            >
                <View style={Styles.formContainerStyle}>
                    <TextInput
                        autoCapitalize="none"
                        onChangeText={setTextSearch}
                        onChange={() => setIsFiltered(false)}
                        value={textSearch}
                        placeholder="Type album name here"
                        style={Styles.textInputStyle}
                    />
                    <Button title="Search" onPress={filteringList} />
                </View>
                <AlbumList
                    albumList={
                        textSearch.length === 0 || !isFiltered
                            ? albumList
                            : filteredList
                    }
                />
            </View>
        </View>
    );
};

const Styles = StyleSheet.create({
    formContainerStyle: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
    },
    textInputStyle: {
        flex: 1,
        borderColor: "#efefef",
        borderWidth: 1,
    },
});

export default MainScreen;
