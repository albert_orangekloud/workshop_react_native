import React from "react";
import { Image } from "react-native";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { BottomTabParamList } from "./types";
import MainScreen from "./MainScreen";
import AccountScreen from "./AccountScreen";

const BottomTabNavigation = () => {
    const BottomTab = createBottomTabNavigator<BottomTabParamList>();

    return (
        <BottomTab.Navigator initialRouteName="Main">
            <BottomTab.Screen
                name="Main"
                component={MainScreen}
                options={{
                    tabBarLabel: "Main",
                    tabBarIcon: ({ color, size }) => {
                        return (
                            <Image
                                source={require("./../../assets/img/datalist.png")}
                                style={{
                                    width: 26,
                                    height: 26,
                                }}
                            />
                        );
                    },
                }}
            />
            <BottomTab.Screen
                name="Account"
                component={AccountScreen}
                options={{
                    tabBarLabel: "Account",
                    tabBarIcon: ({ color, size }) => {
                        return (
                            <Image
                                source={require("./../../assets/img/service.png")}
                                style={{
                                    width: 26,
                                    height: 26,
                                }}
                            />
                        );
                    },
                }}
            />
        </BottomTab.Navigator>
    );
};

export default BottomTabNavigation;
