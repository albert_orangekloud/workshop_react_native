import { BottomTabNavigationProp } from "@react-navigation/bottom-tabs";
import { CompositeNavigationProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";

type HomeStackParamList = {
    BottomTab: undefined;
    GalleryDetail: {
        title: string | null | undefined;
        url: string | null | undefined;
    };
};

type BottomTabParamList = {
    Main: undefined;
    Account: undefined;
};

type ParamList = CompositeNavigationProp<
    StackNavigationProp<HomeStackParamList, "BottomTab">,
    BottomTabNavigationProp<BottomTabParamList, "Main">
>;

export { HomeStackParamList, BottomTabParamList, ParamList };
