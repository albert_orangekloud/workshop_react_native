import React, { useState, useEffect } from "react";
import {
    TextInput,
    useColorScheme,
    View,
    StyleSheet,
    Image,
} from "react-native";

import { RouteProp, useRoute } from "@react-navigation/native";
import { Colors } from "react-native/Libraries/NewAppScreen";

import Button from "../component/Button/Button";
import Header from "../component/Header/Header";
import { HomeStackParamList } from "./types";

type GalleryDetailScreenRouteProp = RouteProp<
    HomeStackParamList,
    "GalleryDetail"
>;

const GalleryDetailScreen = () => {
    const {
        params: { title, url },
    } = useRoute<GalleryDetailScreenRouteProp>();

    return (
        <View style={{ flex: 1, flexDirection: "column" }}>
            <Header title={title ?? ""} />
            <View style={Styles.containerStyle}>
                <View style={Styles.cardSectionStyle}>
                    <Image
                        style={Styles.imageStyle}
                        source={{ uri: url ?? "" }}
                    />
                </View>
            </View>
            <Button />
        </View>
    );
};

const Styles = StyleSheet.create({
    containerStyle: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
    },
    cardSectionStyle: {
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: "#fff",
        justifyContent: "flex-start",
        flexDirection: "row",
        borderColor: "#ddd",
        position: "relative",
    },
    thumbnailStyle: {
        height: 50,
        width: 50,
    },
    headerContentStyle: {
        justifyContent: "space-around",
        flex: 1,
    },
    headerTextStyle: {
        fontSize: 18,
    },
    thumbnailContainerStyle: {
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10,
        marginRight: 10,
    },
    imageStyle: {
        height: 300,
        flex: 1,
    },
});

export default GalleryDetailScreen;
